package com.tbahlai.compose_calendar.month.state

data class CalendarState(
    val monthState: MonthState,
    val modeState: CalendarInfoState
)