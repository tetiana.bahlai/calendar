package com.tbahlai.compose_calendar.month

import androidx.compose.runtime.mutableStateMapOf
import com.tbahlai.compose_calendar.utils.getNextIndex
import com.tbahlai.compose_calendar.utils.getPreviousIndex
import com.tbahlai.compose_calendar.utils.next
import com.tbahlai.compose_calendar.utils.previous
import java.time.YearMonth

class MonthProvider(
    val currentMonth: YearMonth,
    val currentIndex: Int
) {
    val cache = mutableStateMapOf<Int, YearMonth>()

    init {
        cache[getNextIndex(currentIndex)] = currentMonth.next()
        cache[currentIndex] = currentMonth
        cache[getPreviousIndex(currentIndex)] = currentMonth.previous()
    }

    fun getMonthBy(index: Int): YearMonth {
        val month = cache[index]
        requireNotNull(month) { "month can't be null" }
        return month
    }
}