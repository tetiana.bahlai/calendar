package com.tbahlai.compose_calendar.month

import java.time.YearMonth

interface MonthInteractor {
    fun monthChanged(currentMonth: YearMonth)
}