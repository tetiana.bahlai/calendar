package com.tbahlai.compose_calendar.week

interface WeekInteractor {
    fun onClick(name: String)
}