package com.tbahlai.compose_calendar.uimodels

data class UiWeek(
    val days: List<UiDay>,
    val events: List<UiWeekEvent>,
)