package com.tbahlai.compose_calendar.uimodels

data class UiWeekEvent(
    val event: UiEvent,
    val startEventDay: Long,
    val countDays: Long,
    val indexTop: Int
)